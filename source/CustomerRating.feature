Feature: Customer Rating
  As a insurance agent, I want to retrieve a customer rating.
  
  Scenario: Check Customer Rating
    Given a client_customer
    When I make an enquiry using  <firstname>,  <lastname>,  <dateofbirth>,  <engineperformance>
    Then the rating is returned as <result>
    And the following related results are shown
    |exists  |firstname |lastname       |dateofbirth  |rating     |       
    |yes     |Billy     |Mustermann     |02/03/1990   |Malus 8    | 
    |yes     |Max       |Mustermann     |12/12/1973   |Malus 10   |
    |yes     |John      |Snow	        |12/12/1989   |Bonus 5    |
    |no      |Maria     |Snow	        |11/11/1989   |Default (Malus 8)    |